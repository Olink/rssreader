const EventEmitter = require('events');
module.exports = class RssFeed extends EventEmitter {
    constructor(url) {
        super();
        this.url = url;
        this.rss = new (require('rss-parser'));
        this.lastCheck = new Date(1900, 1, 1);
        var that = this;
        this.checkUrl = function() {
            that.emit('scrape');
            that.rss.parseURL(that.url).then(function(result) {
                let items = result.items.reverse();
                result.items.forEach(item => {
                    let pubDate = Date.parse(item.pubDate);
                    if (pubDate > that.lastCheck) {
                        that.emit('new_item', item);
                    };
                });
                that.lastCheck = new Date();
            }, function(err) {
                console.error(err);
            });

            setTimeout(function(){
                that.checkUrl(url);
            }, 5000);
        };
    }

    start() {
        this.checkUrl()
    }
}