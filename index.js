let RSS = require('./rss.js');
var feed = new RSS("http://www.siliconera.com/feed/")
feed.on('new_item', item => {
    console.log(`${item.pubDate}: ${item.title}`);
});

feed.on('scrape', () => {
    console.log(`${new Date()}: Scrape started.`);
});

feed.start();